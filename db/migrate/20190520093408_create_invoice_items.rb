class CreateInvoiceItems < ActiveRecord::Migration[5.2]
  def change
    create_table :invoice_items do |t|
      t.integer :invoice_id
      t.string :label
      t.float :unit_price
      t.integer :quantity

      t.timestamps
    end
    add_index :invoice_items, :invoice_id
  end
end
