class AddEnabledToOrder < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :enabled, :boolean, default: false
  end
end
