class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.string :label
      t.float :unit_price
      t.integer :quantity

      t.timestamps
    end
    add_index :order_items, :order_id
  end
end
