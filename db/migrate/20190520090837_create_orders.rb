class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :number

      t.timestamps
    end
    add_index :orders, :number
  end
end
