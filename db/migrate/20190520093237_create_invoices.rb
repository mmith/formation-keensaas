class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.integer :order_id
      t.string :number

      t.timestamps
    end
    add_index :invoices, :order_id
    add_index :invoices, :number
  end
end
