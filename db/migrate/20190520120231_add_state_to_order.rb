class AddStateToOrder < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :state, :string
    add_index :orders, :state
  end
end
