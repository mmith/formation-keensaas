class CreateLogItems < ActiveRecord::Migration[5.2]
  def change
    create_table :log_items do |t|
      t.text :body
      t.string :loggable_type
      t.integer :loggable_id

      t.timestamps
    end
  end
end
