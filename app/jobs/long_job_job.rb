class LongJobJob < ApplicationJob
  queue_as :low_priority

  after_perform :alert_when_finished

  def perform(*args)
    Order.where(state: 'cancelled').each do |o|
      o.enable
    end
  end

  private
    def alert_when_finished
      ActionCable.server.broadcast 'end_of_job_channel', message: 'ok'
    end
end
