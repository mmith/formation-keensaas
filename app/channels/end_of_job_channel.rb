class EndOfJobChannel < ApplicationCable::Channel
  def subscribed
    stream_from "end_of_job_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
