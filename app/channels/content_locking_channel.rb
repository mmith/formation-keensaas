class ContentLockingChannel < ApplicationCable::Channel
  def subscribed
    stream_from "content_locking_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
