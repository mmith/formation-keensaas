module ApplicationHelper

  def main_sidebar_link *args, &block
    options = args.pop

    title = options.delete(:title)
    url = options.delete(:url)
    icon = options.delete(:icon)
    klass = options.delete(:class) || ''
    active = options.delete(:active) || ''

    if block
      content = capture(&block)
      if klass =~ /tree/
        add = content_tag('span', content_tag('i', '', class: 'fa fa-angle-left pull-right') , class: 'pull-right-container')
      end
      content_tag('li', link_to(content_tag('i','', class: "fa fa-#{icon}") + content_tag('span',title) + add, url, :title => title) + content, class: (klass + ' ' + active))
    else
      content_tag('li', link_to(content_tag('i','', class: "fa fa-#{icon}") + content_tag('span',title) , url, :title => title), class: (klass + ' ' + active))
    end
  end

  def info_box *args, &block
    options = args.pop

    title = options.delete(:title) || 'Title'
    indicator = options.delete(:indicator) || 0
    icon = options.delete(:icon)
    klass = options.delete(:class) || ''
    url = options.delete(:url) || ''
    active = options.delete(:active) || ''

    if block
      content = capture(&block)
    else
      content = ''
    end

    info_box_header = content_tag('div', content_tag('i','', class: 'fa fa-'+icon), class: 'info-box-icon ' + klass )

    span_text = content_tag('span', title, class: 'info-box-text')
    span_number = content_tag('span', indicator, class: 'info-box-number')
    info_box_content = content_tag('div', span_text + span_number + content , class: 'info-box-content')

    link_to content_tag('div', info_box_header + info_box_content, class: 'info-box'), url, style: "color: #333"
  end

  def breadcrumb_box items

    content = ''
    items.each do |item|
      content += content_tag('li', link_to( content_tag('i','', class: 'fa fa-' + item[:icon]) + item[:title], item[:url] ), class: (item == items.last && 'active'))
    end

    content_tag('ol', content.html_safe, class: 'breadcrumb')
  end

  def safe_html unsafe_html
    scrubber = Loofah::Scrubber.new do |node|
      node.remove if node.name == 'script'
    end

    sanitize unsafe_html, scrubber: scrubber
  end

end
