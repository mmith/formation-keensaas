class OrdersController < ApplicationController

  def index
    @orders  = Order.all
  end

  def cancelleds
    LongJobJob.set(wait: 30.seconds).perform_later
    redirect_to orders_path
  end

  def show
    @order = Order.find params[:id]
    ActionCable.server.broadcast 'content_locking_channel', direction: 'lock', order_id: @order.id
  end

  def unlock
    @order = Order.find params[:id]
    ActionCable.server.broadcast 'content_locking_channel', direction: 'unlock', order_id: @order.id
  end
end
