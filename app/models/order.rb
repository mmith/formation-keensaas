class Order < ApplicationRecord
  include Notifiable

  has_many :order_items
  has_one :invoice

  before_save :generate_number

  state_machine :state, initial: :pending do
    before_transition from: :pending, to: :cancelled, do: :notify_admin

    event :enable do
      transition [:pending,:cancelled] => :enabled
    end

    event :cancel do
      transition [:pending, :enabled, :invoiced] => :cancelled
    end
  end

  private
    def generate_number
      self.number = 'O-'+(Order.count +1).to_s
    end

    def generate_invoice
      i = self.create_invoice
      self.order_items.each do |order_item|
        i.invoice_items.create order_item.attributes.delete_if{ |k,v| %w(order_id created_at updated_at).include?(k) }
      end
    end

    def notify_admin
      puts 'order cancelled'
    end
end
