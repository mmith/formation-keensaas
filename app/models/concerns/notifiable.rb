module Notifiable
  extend ActiveSupport::Concern

  included do
    has_many :log_items, as: :loggable
  end

  def log_all_action
    puts 'action'
  end
end
