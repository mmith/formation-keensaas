class LogItem < ApplicationRecord
  belongs_to :loggable, polymorphic: true
end
