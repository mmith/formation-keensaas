App.content_locking = App.cable.subscriptions.create "ContentLockingChannel",
  connected: ->
    #alert('your browser is now connected')

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    if data['direction'] == 'lock'
      $('#order_'+data['order_id']).addClass('locked')
    else if data['direction'] == 'unlock'
      $('#order_'+data['order_id']).removeClass('locked')
