Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resource :dashboard

  resources :orders, only: %w+index show unlock cancelleds+ do
    member do
      get :unlock
    end

    collection do
      get :cancelleds
    end
  end
  root to: 'dashboards#show'
end
