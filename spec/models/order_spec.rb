require 'rails_helper'

RSpec.describe Order, type: :model do

  it{ should have_db_index(:number) }

  it{ should have_many(:order_items) }
  it{ should have_one(:invoice) }

  context "test state machine" do
    before do
      @order = FactoryBot.create(:order)
    end

    it 'should be pending at initial state' do
      expect(@order.pending?).to be_truthy
      expect(@order.enabled?).to be_falsy
      expect(@order.cancelled?).to be_falsy
    end

    it 'should be enabled after enable event from pending' do
      @order.enable
      expect(@order.pending?).to be_falsy
      expect(@order.enabled?).to be_truthy
      expect(@order.cancelled?).to be_falsy
    end

    it 'should be enabled after enable event from cancelled' do
      @order.enable
      @order.cancel
      @order.enable
      expect(@order.pending?).to be_falsy
      expect(@order.enabled?).to be_truthy
      expect(@order.cancelled?).to be_falsy
    end

    it 'should be cancelled after cancel event from pending' do
      @order.cancel
      expect(@order.pending?).to be_falsy
      expect(@order.enabled?).to be_falsy
      expect(@order.cancelled?).to be_truthy
    end
  end

  context "test generate_number callback" do
    before do
      @order = FactoryBot.build(:order)
    end

    it "should generate number before saving" do
      @order.save
      expect(@order.number.is_a?(String)).to be_truthy

    end
  end
end
